import numpy as np
import matplotlib.pyplot as plt

def s1(t):
    return 2*np.sin(2*np.pi*3*t)

def plot_signal(s, N=1000, ta=0, tb=10):
    fig = plt.figure()
    Ts = np.linspace(ta,tb,N)
    plt.plot(Ts, [s(t) for t in Ts])
    plt.grid()
    plt.show()
    
def plot_courbe_enroulee(v , s, N=1000, a=0, b=10):
    plt.figure()
    Ts = np.linspace(a,b,N)
    st = np.array([s(t) for t in Ts])
    pts = np.array([[st[i]*np.cos(2*np.pi*v*Ts[i]),st[i]*np.sin(2*np.pi*v*Ts[i])]for i in range(N)])
    plt.plot(pts[:,0], pts[:,1])
    amplitude = np.max(abs(st))
    plt.xlim(-amplitude, amplitude)
    plt.ylim(-amplitude, amplitude)
    plt.grid()
    m = np.mean(pts, 0)
    plt.plot(m[0],m[1],'ro')
    plt.show()
    
def plot_fournier(s, a, b, N, v0=0, vmax=10, nb_pts=100):
    Ts = np.linspace(a,b,N)
    st = s(Ts)
    Vs = np.linspace(v0,vmax,nb_pts)
    mxs = []
    mys = []
    for v in Vs:
        m = np.mean(np.array([[st[i]*np.cos(2*np.pi*v*Ts[i]),st[i]*np.sin(2*np.pi*v*Ts[i])]for i in range (N)]), 0)
        mxs.append(m[0])
        mys.append(m[1])
    mxs = np.array(mxs)
    mys = np.array(mys)
    fig = plt.figure()
    plt.plot(Vs, mxs, label='x')
    plt.plot(Vs, mys, label='y')
    plt.plot(Vs, (mxs**2 + mys**2)**0.5, label="d")
    plt.legend()
    plt.xticks(range(vmax+1))
    plt.grid()
    plt.show()
    
#plot_courbe_enroulee(6.9,s1,5000)
plot_fournier(s1,0,10,1000)